import sys
import os
sys.path.append(os.path.abspath(os.path.join(__file__, '..', '..', '..')))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

from argparse import ArgumentParser
from logging import getLogger, INFO, Formatter, StreamHandler, WARN
from sllurp.llrp import LLRP_PORT, LLRPClientFactory
from twisted.internet import reactor

import requests
import json

from browser.models import Gtin, Gs1_gcp
from serializers import GtinSerializer, GCP_Serializer

from SGTIN96toGTIN import parse_sgtin_96

logger = getLogger('sllurp')

# https://www.epc-rfid.info/epc-binary-headers
epcCodingScheme = {
        0x00: 'Unprogrammed',
        0x2C: 'GDTI-96',
        0x2D: 'GSRN-96',
        0x2E: 'RFU',
        0x2F: 'USDoD-96',
        0x30: 'SGTIN-96',
        0x31: 'SSCC-96',
        0x32: 'SGLN-96',
        0x33: 'GRAI-96',
        0x34: 'GIAI-96',
        0x35: 'GID-96',
        0x36: 'SGTIN-198',
        0x37: 'GRAI-170',
        0x38: 'GIAI-202',
        0x39: 'SGLN-195',
        0x3A: 'GDTI-113',
        0x3B: 'ADI-var',
}

def setup_logging():
    logger.setLevel(INFO)
    logFormat = '%(asctime)s %(name)s: %(levelname)s: %(message)s'
    formatter = Formatter(logFormat)
    handler = StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    stream_handler_warn = StreamHandler()
    stream_handler_warn.setLevel(WARN)
    stream_handler_warn.setFormatter(formatter)

    access_log = getLogger("tornado.access")
    access_log.addHandler(stream_handler_warn)

    app_log = getLogger("tornado.application")
    app_log.addHandler(handler)

    gen_log = getLogger("tornado.general")
    gen_log.addHandler(handler)

def tag_seen_callback(llrpMsg):
        """Function to run each time the reader reports seeing tags."""
        tags = llrpMsg.msgdict['RO_ACCESS_REPORT']['TagReportData']
        if tags:
            try:
                for tag in tags:
                    if 'EPC-96' in tag:
                        tag['EPC-96'] = tag['EPC-96'].decode('utf-8')
                        schemeId = bytearray.fromhex(tag['EPC-96'])[0]
                        scheme = epcCodingScheme.get(schemeId, 'unknown')
                        tag['CodingScheme'] = scheme
                        if scheme == 'SGTIN-96':
                            parts, URI = parse_sgtin_96(tag['EPC-96'])
                            # 'URI': 'urn:epc:tag:sgtin-96:1.0191906.024344.8641394833'
                            # 'parts': {'header': 48, 'filter': 1, 'partition': '101', 'company_prefix': '0191906', 'item_reference': '024344', 'serial': 8641394833}
                            tag['URI'] = URI
                            tag['parts'] = parts
                            try:
                                gcp = Gs1_gcp.objects.get(pk=parts['company_prefix'])
                                tag['gcp'] = GCP_Serializer(gcp).data
                            except Gs1_gcp.DoesNotExist:
                                pass
                            try:
                                gtin = Gtin.objects.get(pk=parts['company_prefix'] + parts['item_reference'])
                                tag['gtin'] = GtinSerializer(gtin).data
                            except Gtin.DoesNotExist:
                                pass

                payload = {
                    'tags': tags,
                }
                print(payload)
                url = 'https://sockethook.ericbetts.dev/hook/ad_wall'
                r = requests.post(url, json=payload)
            except Exception as e:
                print(e)


def parse_args():
    parser = ArgumentParser(description='Simple RFID Reader Inventory')
    parser.add_argument('host', help='hostname or IP address of RFID reader', nargs='*')
    parser.add_argument('-p', '--port', default=LLRP_PORT, type=int, help='port to connect to (default {})'.format(LLRP_PORT))
    parser.add_argument('-n', '--report-every-n-tags', default=1, type=int, dest='every_n', metavar='N', help='issue a TagReport every N tags')
    parser.add_argument('-a', '--antennas', default='1', help='comma-separated list of antennas to enable')
    parser.add_argument('-X', '--tx-power', default=0, type=int, dest='tx_power', help='Transmit power (default 0=max power)')
    parser.add_argument('-M', '--modulation', default='M8', help='modulation (default M8)')
    parser.add_argument('-T', '--tari', default=0, type=int, help='Tari value (default 0=auto)')
    return parser.parse_args()

def polite_shutdown(factory):
    return factory.politeShutdown()


if __name__ == '__main__':
    setup_logging()

    # Load Sllurp config
    args = parse_args()
    enabled_antennas = list(map(lambda x: int(x.strip()), args.antennas.split(',')))

    # Create Clients and set them to connect
    fac = LLRPClientFactory(report_every_n_tags=args.every_n,
                            antennas=enabled_antennas,
                            tx_power=args.tx_power,
                            start_inventory=True,
                            tari=args.tari,
                            tag_content_selector={
                                'EnableROSpecID': True,
                                'EnableSpecIndex': True,
                                'EnableInventoryParameterSpecID': True,
                                'EnableAntennaID': True,
                                'EnableChannelIndex': True,
                                'EnablePeakRSSI': True,
                                'EnableFirstSeenTimestamp': True,
                                'EnableLastSeenTimestamp': True,
                                'EnableTagSeenCount': True,
                                'EnableAccessSpecID': True,
                            })
    fac.addTagReportCallback(tag_seen_callback)

    for host in args.host:
        reactor.connectTCP(host, args.port, fac, timeout=3)

    reactor.addSystemEventTrigger('before', 'shutdown', polite_shutdown, fac)

    # Start server & connect to readers
    reactor.run()
